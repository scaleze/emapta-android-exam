package net.simplifiedcoding.emampta.data.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.simplifiedcoding.emampta.data.db.AppDatabase
import net.simplifiedcoding.emampta.data.db.entities.Joke
import net.simplifiedcoding.emampta.data.network.MyApi2
import net.simplifiedcoding.emampta.data.network.SafeApiRequest
import net.simplifiedcoding.emampta.data.preferences.PreferenceProvider
import net.simplifiedcoding.emampta.util.Coroutines
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

private val MINIMUM_INTERVAL = 6

class JokesRepository (
    private val api: MyApi2,
    private val db: AppDatabase,
    private val prefs: PreferenceProvider
) : SafeApiRequest() {

    private val jokes = MutableLiveData<List<Joke>>()

    init {
        jokes.observeForever {
            saveJokes(it)
        }
    }

    suspend fun getJokes(): LiveData<List<Joke>> {
        return withContext(Dispatchers.IO){
            fetchJokes()
            db.getJokeDao().getJokes()
        }
    }

    suspend fun getJokesById(
        id: Int
    ): LiveData<Joke> {
        return withContext(Dispatchers.IO){
            fetchJokes()
            db.getJokeDao().getJokesById(id)
        }
    }

    private suspend fun fetchJokes() {
        val lastSavedAt = prefs.getLastSavedAt()

        if (lastSavedAt == null || isFetchNeeded(LocalDateTime.parse(lastSavedAt))) {
            try {
                val response = apiRequest { api.getJokes() }
                jokes.postValue(response.jokes)
            } catch (e: Exception) {

            }
        }
    }

    private fun isFetchNeeded(savedAt: LocalDateTime): Boolean {
        return ChronoUnit.HOURS.between(savedAt, LocalDateTime.now()) > MINIMUM_INTERVAL
    }

    private fun saveJokes(jokes: List<Joke>) {
        Coroutines.io {
            prefs.savelastSavedAt(LocalDateTime.now().toString())
            db.getJokeDao().saveAllJokes(jokes)
        }
    }
}