package net.simplifiedcoding.emampta.data.db.entities

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Joke (
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    val type: String,
    val setup: String?,
    val joke: String?,
    val delivery: String?,
    val category: String,
    @Embedded
    val flags: Flags,
    val safe: Boolean,
    val lang: String
)