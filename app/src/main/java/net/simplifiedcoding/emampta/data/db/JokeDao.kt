package net.simplifiedcoding.emampta.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import net.simplifiedcoding.emampta.data.db.entities.Joke

@Dao
interface JokeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllJokes(jokes: List<Joke>)

    @Query("SELECT * FROM Joke")
    fun getJokes(): LiveData<List<Joke>>

    @Query("SELECT * FROM Joke WHERE id = :id")
    fun getJokesById(id: Int): LiveData<Joke>
}