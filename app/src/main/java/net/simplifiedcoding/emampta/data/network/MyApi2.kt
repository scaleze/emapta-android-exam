package net.simplifiedcoding.emampta.data.network

import net.simplifiedcoding.emampta.data.network.responses.JokesResponse
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface MyApi2 {

    @GET("Programming")
    suspend fun getJokes(
        @Query("amount") amount: Int = 10
    ) : Response<JokesResponse>

    companion object{
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ) : MyApi2{

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl("https://v2.jokeapi.dev/joke/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyApi2::class.java)
        }
    }

}

