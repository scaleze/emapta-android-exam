package net.simplifiedcoding.emampta.data.network.responses

import net.simplifiedcoding.emampta.data.db.entities.Joke

data class JokesResponse (
    val error: Boolean,
    val amount: Int,
    val jokes: List<Joke>
)