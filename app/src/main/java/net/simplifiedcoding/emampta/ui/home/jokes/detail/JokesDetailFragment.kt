package net.simplifiedcoding.emampta.ui.home.jokes.detail

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.jokes_detail_fragment.*
import kotlinx.android.synthetic.main.jokes_detail_fragment.progress_bar
import kotlinx.android.synthetic.main.jokes_fragment.*
import net.simplifiedcoding.emampta.R
import net.simplifiedcoding.emampta.util.Coroutines
import net.simplifiedcoding.emampta.util.hide
import net.simplifiedcoding.emampta.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.factory
import java.util.*

class JokesDetailFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val viewModelFactoryInstanceFactory: ((Int) -> JokesDetailViewModelFactory) by factory()

    private lateinit var viewModel: JokesDetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.jokes_detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val safeArgs = arguments?.let { JokesDetailFragmentArgs.fromBundle(it) }
        val id = safeArgs?.id

        viewModel = ViewModelProvider(this, viewModelFactoryInstanceFactory(id!!)).get(JokesDetailViewModel::class.java)
        bindUI()
    }

    private fun bindUI() = Coroutines.main {
        progress_bar.show()
        viewModel.jokes.await().observe(viewLifecycleOwner, Observer {
            progress_bar.hide()
            category.text = it.category
            lang.text = it.lang.toUpperCase(Locale.getDefault())
            type.text = it.type
            joke.text = it.joke
            setup.text = it.setup
            delivery.text = it.delivery
            safe.isChecked = it.safe
            nsfw.isChecked = it.flags.nsfw
            religious.isChecked = it.flags.religious
            political.isChecked = it.flags.political
            racist.isChecked = it.flags.racist
            sexist.isChecked = it.flags.sexist
            explicit.isChecked = it.flags.explicit
        })
    }

}