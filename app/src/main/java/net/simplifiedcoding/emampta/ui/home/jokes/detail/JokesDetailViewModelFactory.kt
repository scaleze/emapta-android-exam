package net.simplifiedcoding.emampta.ui.home.jokes.detail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.simplifiedcoding.emampta.data.repositories.JokesRepository

@Suppress("UNCHECKED_CAST")
class JokesDetailViewModelFactory(
    private val id: Int,
    private val repository: JokesRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return JokesDetailViewModel(id, repository) as T
    }
}