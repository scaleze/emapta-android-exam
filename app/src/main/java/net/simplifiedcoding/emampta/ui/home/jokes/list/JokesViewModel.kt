package net.simplifiedcoding.emampta.ui.home.jokes.list

import androidx.lifecycle.ViewModel
import net.simplifiedcoding.emampta.data.repositories.JokesRepository
import net.simplifiedcoding.emampta.util.lazyDeferred

class JokesViewModel(
    repository: JokesRepository
) : ViewModel() {

    val jokes by lazyDeferred {
        repository.getJokes()
    }
}