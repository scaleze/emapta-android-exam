package net.simplifiedcoding.emampta.ui.home.jokes.detail

import androidx.lifecycle.ViewModel
import net.simplifiedcoding.emampta.data.repositories.JokesRepository
import net.simplifiedcoding.emampta.util.lazyDeferred

class JokesDetailViewModel(
    private val id: Int,
    repository: JokesRepository
) : ViewModel() {
    val jokes by lazyDeferred {
        repository.getJokesById(id)
    }
}