package net.simplifiedcoding.emampta.ui.home.jokes.list

import com.xwray.groupie.databinding.BindableItem
import net.simplifiedcoding.emampta.R
import net.simplifiedcoding.emampta.data.db.entities.Joke
import net.simplifiedcoding.emampta.databinding.ItemJokeBinding

class JokeItem(
    val joke: Joke
) : BindableItem<ItemJokeBinding>(){

    override fun getLayout() = R.layout.item_joke

    override fun bind(viewBinding: ItemJokeBinding, position: Int) {
        viewBinding.joke = joke
    }
}