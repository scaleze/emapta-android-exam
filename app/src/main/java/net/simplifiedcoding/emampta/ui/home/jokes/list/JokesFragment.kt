package net.simplifiedcoding.emampta.ui.home.jokes.list

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.jokes_fragment.*
import net.simplifiedcoding.emampta.R
import net.simplifiedcoding.emampta.data.db.entities.Joke
import net.simplifiedcoding.emampta.util.Coroutines
import net.simplifiedcoding.emampta.util.hide
import net.simplifiedcoding.emampta.util.show
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class JokesFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val factory: JokesViewModelFactory by instance()

    private lateinit var viewModel: JokesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.jokes_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this, factory).get(JokesViewModel::class.java)
        bindUI()
    }

    private fun bindUI() = Coroutines.main {
        progress_bar.show()
        viewModel.jokes.await().observe(viewLifecycleOwner, Observer {
            progress_bar.hide()
            initRecyclerView(it.toJokeItem())
        })
    }

    private fun initRecyclerView(quoteItem: List<JokeItem>) {

        val mAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(quoteItem)
        }

        recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }

        mAdapter.setOnItemClickListener { item, view ->
            (item as? JokeItem)?.let {
                showWeatherDetail(it.joke.id, view)
            }
        }

    }

    private fun List<Joke>.toJokeItem(): List<JokeItem> {
        return this.map {
            JokeItem(it)
        }
    }

    private fun showWeatherDetail(id: Int, view: View) {
        val actionDetail = JokesFragmentDirections.actionJokesFragmentToJokesDetailFragment(id)
        Navigation.findNavController(view).navigate(actionDetail)
    }

}