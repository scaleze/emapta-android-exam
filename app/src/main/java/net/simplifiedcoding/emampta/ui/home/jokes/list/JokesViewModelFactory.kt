package net.simplifiedcoding.emampta.ui.home.jokes.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.simplifiedcoding.emampta.data.repositories.JokesRepository

@Suppress("UNCHECKED_CAST")
class JokesViewModelFactory(
    private val repository: JokesRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return JokesViewModel(repository) as T
    }
}