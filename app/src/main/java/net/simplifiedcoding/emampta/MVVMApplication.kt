package net.simplifiedcoding.emampta

import android.app.Application
import net.simplifiedcoding.emampta.data.db.AppDatabase
import net.simplifiedcoding.emampta.data.network.MyApi2
import net.simplifiedcoding.emampta.data.network.NetworkConnectionInterceptor
import net.simplifiedcoding.emampta.data.preferences.PreferenceProvider
import net.simplifiedcoding.emampta.data.repositories.JokesRepository
import net.simplifiedcoding.emampta.ui.home.jokes.detail.JokesDetailViewModelFactory
import net.simplifiedcoding.emampta.ui.home.jokes.list.JokesViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.*

class MVVMApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MVVMApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi2(instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { JokesRepository(instance(), instance(), instance()) }
        bind() from provider{ JokesViewModelFactory(instance()) }
        bind() from factory {id: Int -> JokesDetailViewModelFactory(id, instance())}


    }

}